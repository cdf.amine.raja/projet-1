#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Amine RAJA (amineraja82004@gmail.com)

:copyright:
      Amine RAJA (amineraja82004@gmail.com)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

"""
from tkinter import Button, Entry, Label, NE, NW, Tk
from obspy import UTCDateTime
from obspy.clients.fdsn import Client
import numpy
# Vas introduire un bloc d’instructions


class Piseenv:

    def __init__(self):
        self.creation_fenetre()

    def creation_fenetre(self):
        # Debut du bloc d'instructions
        self.fenetre = Tk()
        self.fenetre.title("PISENV")
        self.fenetre.geometry("600x800")

        # Création du label Durée
        la = Label(self.fenetre, text="Durée:")
        la.place(anchor=NW)
        self.duree = Entry(self.fenetre, width=30)
        self.duree.place(x=50, y=2)
        self.duree.insert(0, '14400')
        # Création du label URL
        Label(self.fenetre, text="URL:").place(x=1, y=35)
        self.URL = Entry(self.fenetre, width=30)
        self.URL.place(x=50, y=35)
        self.URL.insert(0, 'http://130.79.11.47:8880')

        # Création du bouton calcule
        self.b1 = Button(self.fenetre, text="Calculer",
                         command=self.recup_convert)
        self.b1.place(relx=1, x=-2, y=2, anchor=NE)

        # Création du label Température, Consigne et Mesure
        Label(self.fenetre, text="Température [deg C°]").place(x=200, y=110)
        Label(self.fenetre, text="Consigne").place(x=70, y=80)
        Label(self.fenetre, text="Mesure").place(x=300, y=80)

        # Création du label Moyenne, Minimum et Maximum
        Label(self.fenetre, text="Moyenne:").place(x=1, y=150)
        Label(self.fenetre, text="Minimum:").place(x=1, y=191)
        Label(self.fenetre, text="Maximum:").place(x=1, y=232)

        #  Création des barres de rechcehrches
        self.T_minimum_consigne = Entry(self.fenetre, width=9)
        self.T_minimum_consigne.place(x=70, y=191)
        self.T_minimum_consigne.insert(0, '20')
        self.T_maximum_consigne = Entry(self.fenetre, width=9)
        self.T_maximum_consigne.place(x=70, y=232)
        self.T_maximum_consigne.insert(0, '24')

        # Création de plusieur labels
        self.T_moyenne_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.T_moyenne_mesure.place(x=300, y=150)

        self.T_minimum_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.T_minimum_mesure.place(x=300, y=191)

        self.T_maximum_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.T_maximum_mesure.place(x=300, y=232)

        # création du labels Humidité, Consigne et Mesure
        Label(self.fenetre, text="Humidité [%]").place(x=200, y=300)

        # création du labels Moyenne, Minimum et Maximum
        Label(self.fenetre, text="Moyenne:").place(x=1, y=386)
        Label(self.fenetre, text="Minimum:").place(x=1, y=427)
        Label(self.fenetre, text="Maximum:").place(x=1, y=468)

        # création de plusieur barre de recherche
        self.H_minimum_consigne = Entry(self.fenetre, width=9)
        self.H_minimum_consigne.place(x=70, y=427)
        self.H_minimum_consigne.insert(0, '20')
        self.H_maximum_consigne = Entry(self.fenetre, width=9)
        self.H_maximum_consigne.place(x=70, y=468)
        self.H_maximum_consigne.insert(0, '80')

        # création de plusieur labels
        self.H_moyenne_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.H_moyenne_mesure.place(x=300, y=386)

        self.H_minimum_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.H_minimum_mesure.place(x=300, y=427)

        self.H_maximum_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.H_maximum_mesure.place(x=300, y=468)

        # création du labels Pression
        Label(self.fenetre, text="Pression [mbar]").place(x=200, y=500)

        # création du labels Moyenne, Minimum, Maximum
        Label(self.fenetre, text="Moyenne:").place(x=1, y=586)
        Label(self.fenetre, text="Minimum:").place(x=1, y=627)
        Label(self.fenetre, text="Maximum:").place(x=1, y=668)

        #  Création des barres de rechcehrches
        self.P_minimum_consigne = Entry(self.fenetre, width=9)
        self.P_minimum_consigne.place(x=70, y=627)
        self.P_minimum_consigne.insert(0, '800')
        self.P_maximum_consigne = Entry(self.fenetre, width=9)
        self.P_maximum_consigne.place(x=70, y=668)
        self.P_maximum_consigne.insert(0, '1100')

        # création de plusieurs labels
        self.P_moyenne_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.P_moyenne_mesure.place(x=300, y=586)

        self.P_minimum_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.P_minimum_mesure.place(x=300, y=627)

        self.P_maximum_mesure = Label(self.fenetre,
                                      width=9, fg="black", bg="white")
        self.P_maximum_mesure.place(x=300, y=668)

        # Création de voyant en utilisant des labels
        self.label_T = Label(self.fenetre, text="bon", fg="black", bg="green")
        self.label_H = Label(self.fenetre, text="bon", fg="black", bg="green")
        self.label_P = Label(self.fenetre, text="bon", fg="black", bg="green")

        self.label_T.place(x=500, y=170)
        self.label_H.place(x=500, y=470)
        self.label_P.place(x=500, y=627)

    # fin du bloc d'instructions

    def recup_convert(self):
        # Les données qu'on veut
        url = self.URL.get()
        net = 'XX'
        sta = 'GPIL'
        loc = '00'
        cha = 'LKI'
        fin = UTCDateTime.utcnow()
        duree = self.duree.get()
        debut = fin-int(duree)

        # demander plusieurs données
        client = Client(base_url=url)
        tr = client.get_waveforms(net, sta, loc, cha, debut, fin)[0]
        for cha in ['LKI', 'LII', 'LDO']:
            tr = client.get_waveforms(net, sta, loc, cha, debut, fin)[0]

            # Formule pour conversion des données en température,
            # Pression et Humidité.

            if cha == 'LKI':
                tr.data = 24*tr.data/1e6-40
                (moy, mini, maxi) = self.stats(tr)
                self.T_moyenne_mesure['text'] = '%.1f' % moy
                self.T_minimum_mesure['text'] = '%.1f' % mini
                self.T_maximum_mesure['text'] = '%.1f' % maxi

                if maxi > float(self.T_maximum_consigne.get()):
                    self.label_T['bg'] = 'red'
                    self.label_T['text'] = 'ko'
                elif mini < float(self.T_minimum_consigne.get()):
                    self.label_T['bg'] = 'red'
                    self.label_T['text'] = 'ko'
                else:
                    self.label_T['bg'] = 'green'
                    self.label_T['text'] = 'ok'

                    # Pression
            if cha == 'LDO':
                tr.data = 60*tr.data/1e6+800
                (moy, mini, maxi) = self.stats(tr)
                self.P_moyenne_mesure['text'] = '%d' % round(moy)
                self.P_minimum_mesure['text'] = '%d' % round(mini)
                self.P_maximum_mesure['text'] = '%d' % round(maxi)

                if maxi > float(self.P_maximum_consigne.get()):
                    self.label_P['bg'] = 'red'
                    self.label_P['text'] = 'ko'
                elif mini < float(self.P_minimum_consigne.get()):
                    self.label_P['bg'] = 'red'
                    self.label_P['text'] = 'ko'
                else:
                    self.label_P['bg'] = 'green'
                    self.label_P['text'] = 'ok'
                # Humidité.
            if cha == 'LII':
                tr.data = 10*tr.data/1e6
                (moy, mini, maxi) = self.stats(tr)
                self.H_moyenne_mesure['text'] = '%d' % round(moy)
                self.H_minimum_mesure['text'] = '%d' % round(mini)
                self.H_maximum_mesure['text'] = '%d' % round(maxi)

                if maxi > float(self.H_maximum_consigne.get()):
                    self.label_H['bg'] = 'red'
                    self.label_H['text'] = 'ko'
                elif mini < float(self.H_minimum_consigne.get()):
                    self.label_H['bg'] = 'red'
                    self.label_H['text'] = 'ko'
                else:
                    self.label_H['bg'] = 'green'
                    self.label_H['text'] = 'ok'

        # Fonction qui calcule la moyenne, minimum et maximume.
    def stats(self, tr):
        return numpy.mean(tr.data), tr.data.min(), tr.data.max()


if __name__ == '__main__':
    # cela implique que le module est exécuté de manière autonome par
    # l utilisateur et que nous pouvons effectuer les actions appropriées

            Piseenv = Piseenv()
            Piseenv.fenetre.mainloop()
